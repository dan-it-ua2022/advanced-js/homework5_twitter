const loader = document.querySelector('.loader');
const content = document.querySelector('.content');
const addCardBtn = document.querySelector('.add__card-btn');

addCardBtn.addEventListener('click', addCardHandler);

const parentCard = 'content__post';

class CardData {
  constructor(userId, name, email, title, description) {
    this.userId = userId;
    this.name = name;
    this.email = email;
    this.title = title;
    this.description = description;
  }
}

class Card {
  constructor(card) {
    this.card = card;
    this.li = null;
  }

  createIn(element) {
    // create card layout
    this.li = document.createElement('li');
    this.li.classList.add(parentCard);
    this.li.dataset.userId = this.card.userId;
    this.li.dataset.postId = this.card.postId;

    const button = document.createElement('button');
    button.classList.add('post__delete');
    button.innerText = 'X';
    button.addEventListener('click', deleteCardHandler);
    this.li.append(button);

    const divUserPost = document.createElement('div');
    divUserPost.classList.add('post__user');
    const spanUserName = document.createElement('span');
    spanUserName.classList.add('user__name');
    spanUserName.innerText = `${this.card.userName} #${this.card.userId}`;
    divUserPost.append(spanUserName);
    const spanUserEmail = document.createElement('span');
    spanUserEmail.classList.add('user__email');
    spanUserEmail.innerText = `${this.card.userEmail}`;
    divUserPost.append(spanUserEmail);
    this.li.append(divUserPost);

    const title = document.createElement('h3');
    title.classList.add('post__title');
    title.innerText = `#${this.card.postId}  ${this.card.postTitle}`;
    this.li.append(title);

    const description = document.createElement('div');
    description.classList.add('post__description');
    description.innerText = this.card.description;
    this.li.append(description);

    element.append(this.li);
  }
}

class CardsListView {
  element;
  dataService;

  constructor(element) {
    this.element = element;
    dataService = dataService;
  }

  #drawList(cardElements) {
    this.element.innerHTML = '';
    cardElements.forEach((cardElement) => {
      cardElement.createIn(this.element);
    });
  }

  drawAll() {
    showLoader();
    const cardElements = [];
    let userAll = [];

    dataService.allUsers.then((users) => {
      if (users.length === 0) return;
      userAll = [...users];
    });

    dataService.allPosts.then((posts) => {
      if (posts.length === 0) return;
      posts.forEach((post) => {
        let user = userAll.find((item) => item.userId === post.userId);
        Object.assign(post, user);
        cardElements.push(new Card(post));
      });
      this.#drawList(cardElements);
      hideLoader();
    });
  }
}

let dataService = {
  urlUsers: 'https://ajax.test-danit.com/api/json/users',
  urlPosts: 'https://ajax.test-danit.com/api/json/posts',

  get allUsers() {
    return fetch(this.urlUsers)
      .then((response) => response.json())
      .then((json) => this.mapArrayUsers(json))
      .catch((error) => console.log(error));
  },

  get allPosts() {
    return fetch(this.urlPosts)
      .then((response) => response.json())
      .then((json) => this.mapArrayPosts(json))
      .catch((error) => console.log(error));
  },

  delete(postId) {
    return fetch(`${this.urlPosts}/${postId}`, {
      method: 'DELETE',
    })
      .then((response) => response.ok)
      .catch((error) => console.log(error));
  },

  add(post) {
    return fetch(this.urlPosts, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userId: post.userId,
        title: `${post.title}`,
        body: `${post.description}`,
      }),
    })
      .then((response) => response.json())
      .then((json) => console.log(json))
      .catch((error) => console.log(error));
  },

  mapArrayUsers(array) {
    return array.map((item) => {
      return {
        userId: item.id,
        userName: item.name,
        userEmail: item.email,
      };
    });
  },

  mapArrayPosts(array) {
    // console.log(array);
    return array.map((item) => {
      return {
        postId: item.id,
        userId: item.userId,
        postTitle: item.title,
        description: item.body,
      };
    });
  },
};

function showLoader() {
  loader.classList.add('show');
}

function hideLoader() {
  loader.classList.remove('show');
}

function deleteCardHandler(e) {
  let deletePost = e.target.closest(`.${parentCard}`);
  let deleteId = deletePost.dataset.postId;

  dataService
    .delete(deleteId)
    .then((response) =>
      response ? deletePost.remove() : console.log('Erorr delete card')
    )
    .catch((error) => console.log(error));
}

function addCardHandler() {
  let userId = 1;
  let name = 'Dima';
  let email = 'a@a.com';
  let title = 'Test text';
  let description = 'ldjflaj lfj laj flkjs lfja';

  const newCard = new CardData(userId, name, email, title, description);

  dataService.add(newCard).then(() => cardsListView.drawAll());
}

let cardsListView = new CardsListView(content);

window.addEventListener('load', function () {
  cardsListView.drawAll();
});
